import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';

class Generate extends StatefulWidget {
  const Generate({Key? key}) : super(key: key);

  @override
  State<Generate> createState() => _GenerateState();
}

class _GenerateState extends State<Generate> {
  // đăng ký widget
  final registry = JsonWidgetRegistry.instance;

  // dữ liệu của widget
  late Future<Map<dynamic, dynamic>> jsonData;

  @override
  void initState() {
    super.initState();
    jsonData = readJson();
  }

  // đọc dữ liệu từ file
  Future<Map<dynamic, dynamic>> readJson() async {
    final String res = await rootBundle.loadString('/dynamic_widgets.json');
    final data = json.decode(res);
    return data;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Map<dynamic, dynamic>>(
      future: jsonData,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (snapshot.hasData) {
          final widgetData = snapshot.data!;

          // build
          final widget =
              JsonWidgetData.fromDynamic(widgetData, registry: registry);
          return Container(
            child: widget!.build(context: context),
          );
        } else if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        }
        return Container();
      },
    );
  }
}
