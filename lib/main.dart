import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fn_generate_json/generate.dart';
import 'package:fn_generate_json/product.dart';
import 'package:fn_generate_json/user_model.dart';
import 'package:json_dynamic_widget/json_dynamic_widget.dart';

// run
// dart run build_runner build --delete-conflicting-outputs

void main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const userData =
      '[{"name": "John Smith", "email": "john@example.com"}, {"name": "Jane Smith", "email": "jane@example.com"}]';
  static const productData =
      '[{"id": "1", "name": "Cocktail", "original": "Ha Noi", "price": "20"}, {"id": "2", "name": "Bread", "original": "Hai Duong", "price": "50"}]';

  Future<List<User>> getUser() async {
    return await Future.delayed(const Duration(seconds: 2), () {
      final data = jsonDecode(userData) as List;
      final user = data.map((e) => User.fromJson(e)).toList();
      return user;
    });
  }

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Demo',
      home: Generate(),
      // home: widget!.build(context: context),
    );
  }

  FutureBuilder<List<User>> fetchProducts() {
    return FutureBuilder(
      future: getUser(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return const Center(child: Text('Error'));
        } else if (snapshot.hasData) {
          final user = snapshot.data as List;

          return ListView.builder(
            itemCount: user.length,
            itemBuilder: (context, index) {
              return Column(children: [
                Text(user[index].name),
              ]);
            },
          );
        } else {
          return Container();
        }
      },
    );
  }
}
