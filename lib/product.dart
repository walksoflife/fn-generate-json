import 'dart:convert';

class Product {
  int id;
  String name, original, price;

  Product(
      {required this.id,
      required this.name,
      required this.price,
      required this.original});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'original': original,
      'price': price,
    };
  }

  String toJson() => json.encode(toMap());

  factory Product.fromMap(Map<String, dynamic> map) {
    return Product(
        id: map["id"],
        name: map["name"],
        original: map["original"],
        price: map["price"]);
  }

  // for object
  factory Product.fromJson(String source) {
    return Product.fromMap(json.decode(source));
  }

  // for list
  static List<Product> fromListJson(String source) {
    final List<dynamic> list = json.decode(source);
    return list.map((e) => Product.fromMap(e)).toList();
  }
}
